#-------------------------------------------------
#
# Project created by QtCreator 2014-11-14T21:57:12
#
#-------------------------------------------------

QT       += core gui opengl

LIBS     += -lkdeui -lglut -lGLU

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets



TARGET = QLetItGraph
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    myglwidget.cpp \
    node.cpp \
    line.cpp \
    about.cpp

HEADERS  += mainwindow.h \
    myglwidget.h \
    node.h \
    line.h \
    about.h

FORMS    += mainwindow.ui \
    about.ui

TRANSLATIONS += qletitgraph_en.ts qletitgraph_fr.ts

RESOURCES += \
    resource.qrc
