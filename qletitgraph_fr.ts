<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="fr_FR">
<context>
    <name>About</name>
    <message>
        <location filename="about.ui" line="14"/>
        <source>About QLetItGraph</source>
        <translation>À propos de QLetItGraph</translation>
    </message>
    <message>
        <location filename="about.ui" line="51"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:14pt; font-weight:600;&quot;&gt;QLetItGraph&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="58"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt; font-weight:600;&quot;&gt;Version 0.1&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="65"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-size:9pt; font-weight:600;&quot;&gt;Using Qt 4.8&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="82"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="about.ui" line="88"/>
        <source>Graph Viewer and Editor</source>
        <translation>Visionneur et Dessin de Graphe</translation>
    </message>
    <message>
        <location filename="about.ui" line="95"/>
        <location filename="about.ui" line="98"/>
        <source>https://bitbucket.org/cedbonhomme/qletitgraph</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="106"/>
        <source>Authors</source>
        <translation>Auteurs</translation>
    </message>
    <message>
        <location filename="about.ui" line="130"/>
        <source>Cedric Bonhomme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="137"/>
        <source>Thibaud Laurent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="144"/>
        <source>Dimitri Prestat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="151"/>
        <source>Mathieu Perroy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="158"/>
        <source>Davy Dupuy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="about.ui" line="165"/>
        <source>Nicolas Sirac</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>QLetItGraph</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="86"/>
        <source>&amp;File</source>
        <translation>&amp;Fichier</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="97"/>
        <source>&amp;Edit</source>
        <translation>&amp;Édition</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="102"/>
        <source>&amp;View</source>
        <translation>&amp;Affichage</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="123"/>
        <source>&amp;Help</source>
        <translation>&amp;Aide</translation>
    </message>
    <message>
        <source>&amp;Settings</source>
        <translation type="obsolete">&amp;Configuration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="147"/>
        <source>&amp;Tools</source>
        <translation>&amp;Outils</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="152"/>
        <source>&amp;Graph</source>
        <translation>&amp;Graphe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="166"/>
        <source>Toolbar</source>
        <translation>Barre principale</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="222"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="235"/>
        <location filename="mainwindow.ui" line="250"/>
        <source>Type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="47"/>
        <source>Node</source>
        <translation>Noeud</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="276"/>
        <source>id</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="320"/>
        <source>Geometry</source>
        <translation>Géométrie</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="335"/>
        <source>Position</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="129"/>
        <source>Setti&amp;ngs</source>
        <translation>&amp;Configuration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="109"/>
        <source>&amp;View Mode</source>
        <translation>&amp;Mode d&apos;affichage</translation>
    </message>
    <message>
        <source>&amp;X</source>
        <translation type="obsolete">X</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="365"/>
        <source>Y</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="387"/>
        <source>Size</source>
        <translation>Taille</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="407"/>
        <source>W</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="417"/>
        <source>H</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="444"/>
        <source>Apparence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="462"/>
        <location filename="mainwindow.ui" line="617"/>
        <source>Color</source>
        <translation>Couleur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="632"/>
        <source>Bac&amp;kground</source>
        <translation>Fond</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="517"/>
        <source>Nodes</source>
        <translation>Noeuds</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="133"/>
        <source>&amp;Configure/Developer</source>
        <translation>&amp;Configuration/Développeur</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="257"/>
        <location filename="mainwindow.cpp" line="58"/>
        <source>Graph</source>
        <translation>Graphe</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="296"/>
        <location filename="mainwindow.cpp" line="48"/>
        <source>node1</source>
        <translation>noeud1</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="355"/>
        <source>X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="477"/>
        <source>Background</source>
        <translation>Fond</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="554"/>
        <source>Lines</source>
        <translation>Lignes</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="672"/>
        <source>Borders</source>
        <translation>Bords</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="710"/>
        <source>Rectangle</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="728"/>
        <source>Border</source>
        <translation>Bord</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="767"/>
        <source>Radius</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="812"/>
        <source>Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="827"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="850"/>
        <source>avatar.png</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="889"/>
        <source>Navigation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="899"/>
        <source>Rotation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="907"/>
        <location filename="mainwindow.ui" line="1071"/>
        <source>x</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="955"/>
        <location filename="mainwindow.ui" line="1101"/>
        <source>y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1003"/>
        <source>z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1254"/>
        <source>&amp;About QLetItGraph</source>
        <translation>À &amp;propos de QLetItGraph</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1265"/>
        <source>&amp;Select Node</source>
        <translation>&amp;Sélectionner un Noeud</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1276"/>
        <source>Select &amp;Graph</source>
        <translation>Sélectioner le &amp;Graphe</translation>
    </message>
    <message>
        <source>&amp;x</source>
        <translation type="obsolete">x</translation>
    </message>
    <message>
        <source>&amp;y</source>
        <translation type="obsolete">y</translation>
    </message>
    <message>
        <source>&amp;z</source>
        <translation type="obsolete">z</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1063"/>
        <source>Translation</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1157"/>
        <source>&amp;Open...</source>
        <translation>&amp;Ouvrir...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1160"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1170"/>
        <source>&amp;Quit</source>
        <translation>&amp;Quitter</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1173"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1186"/>
        <source>&amp;Save</source>
        <translation>&amp;Enregistrer</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1189"/>
        <source>Ctrl+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1199"/>
        <source>Save &amp;As...</source>
        <translation>Enregistrer &amp;sous...</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1202"/>
        <source>Ctrl+Shift+S</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1212"/>
        <source>&amp;Properties</source>
        <translation>&amp;Propriétés</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1215"/>
        <source>Alt+Return</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1228"/>
        <source>&amp;2D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="1244"/>
        <source>&amp;3D</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Open Graph</source>
        <translation>Ouvrir Graphe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="34"/>
        <source>Graph Files (*.dot *.graphml)</source>
        <translation>Fichiers graphe (*.dot *.graphml)</translation>
    </message>
</context>
<context>
    <name>MyGLWidget</name>
    <message>
        <source>Delete</source>
        <translation type="obsolete">Supprimer</translation>
    </message>
    <message>
        <location filename="myglwidget.cpp" line="114"/>
        <source>Delete Node</source>
        <translation>Supprimer Noeud</translation>
    </message>
    <message>
        <location filename="myglwidget.cpp" line="115"/>
        <source>New line</source>
        <translation>Nouvelle ligne</translation>
    </message>
    <message>
        <location filename="myglwidget.cpp" line="116"/>
        <source>Delete line</source>
        <translation>Supprimer ligne</translation>
    </message>
    <message>
        <location filename="myglwidget.cpp" line="117"/>
        <source>New Node</source>
        <translation>Nouveau Noeud</translation>
    </message>
    <message>
        <location filename="myglwidget.cpp" line="118"/>
        <source>Properties</source>
        <translation>Propriétés</translation>
    </message>
</context>
</TS>
