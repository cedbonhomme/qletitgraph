#ifndef MYGLWIDGET_H
#define MYGLWIDGET_H

#include <QGLWidget>
#include <node.h>
#include <QList>


class MyGLWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit MyGLWidget(QWidget *parent = 0);

protected:
    void initializeGL();
    void paintGL();
    void resizeGL(int width, int height);

    QSize minimumSizeHint() const;
    QSize sizeHint() const;
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void contextMenuEvent(QContextMenuEvent *event);
    void wheelEvent(QWheelEvent *event);

signals:
    void xRotationChanged(int angle);
    void yRotationChanged(int angle);
    void zRotationChanged(int angle);
    void xTranslationChanged(int trans);
    void yTranslationChanged(int trans);
    void zTranslationChanged(int trans);
    void backgroundColorChanged(QColor color);

    void lignColorChanged(QColor color);
    void nodeColorChanged(QColor color);

    void scaleChanged(int scale);

public slots:
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);
    void setXTranslation(int trans);
    void setYTranslation(int trans);
    void setZTranslation(int trans);
    void setBackgroundColor(QColor color);

    void setLignColor(QColor color);
    void setNodeColor(QColor color);

    void setScale(int scale);

private:
    int xRot;
    int yRot;
    int zRot;

    int xTrans;
    int yTrans;
    int zTrans;

    QColor backgroundColor;
    QColor nodeColor;
    QColor lignColor;

    float scale;

    QPoint lastPos;

    QList<Node *> listNodes;

    void draw();
};

#endif // MYGLWIDGET_H
