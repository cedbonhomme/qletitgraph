#include "node.h"

Node::Node()
{

}

Node::Node(QVector3D topLeft, QVector3D topRight, QVector3D bottomRight, QVector3D bottomLeft, QColor color )
{
    this->topLeft = topLeft;
    this->topRight = topRight;
    this->bottomRight = bottomRight;
    this->bottomLeft = bottomLeft;
    this->color = color;
    this->hitBox = new QRect( this->topLeft.toPoint(), this->bottomRight.toPoint() );
}

Node::Node(QVector3D topLeft, float width, float height, QColor color)
{
    this->topLeft = topLeft;
    this->topRight = QVector3D( topLeft.x() + width ,topLeft.y(),  topLeft.z() );
    this->bottomRight = QVector3D( topLeft.x() + width ,topLeft.y() - height,  topLeft.z() );
    this->bottomLeft = QVector3D( topLeft.x() ,topLeft.y() - height,  topLeft.z() );
    this->color = color;
    this->hitBox = new QRect( this->topLeft.toPoint(), this->bottomRight.toPoint() );
}

Node::~Node()
{

}


